export const SET_COMMENTS = 'setComments';
export const ADD_COMMENT = 'addComment';
export const SET_COMMENT_IMAGE = 'setCommentImage';
export const SET_COMMENT = 'editComment';
export const DELETE_COMMENT = 'deleteComment';
